module.exports = {
	apps : [{
		name      : 'core/toolbox',
		script    : 'build/toolbox.js',
		env: {
			// Either inject the values via environment variables or define them here
            TRANSPORT_BIND_ADDRESS: process.env.TRANSPORT_BIND_ADDRESS || ""
		}
	}]
};
