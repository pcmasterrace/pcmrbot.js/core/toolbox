import { Service, Context, Errors } from "moleculer";
import * as pako from "pako";
import * as Base64 from "base-64";

import * as Interfaces from "./interfaces";

class UsernotesService extends Service {
	constructor(broker) {
		super(broker);

		this.parseServiceSchema({
			name: "toolbox.usernotes",
			version: 1,
			dependencies: [
				{name: "reddit.wiki", version: 1}
			],
            actions: {
				getUsernotes: {
					name: "getUsernotes",
					params: {
						subreddit: "string",
						user: {type: "string", optional: true}
					},
					handler: this.getUsernotes
				},
				addUsernote: {
					name: "addUsernote",
					params: {
						subreddit: "string",
						user: "string",
						
					}
				}
			}
        });
    }
}

module.exports = UsernotesService;