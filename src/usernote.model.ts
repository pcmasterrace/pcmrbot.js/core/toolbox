import * as Sequelize from "sequelize";

module.exports = function(sequelize: Sequelize.Sequelize, DataTypes: Sequelize.DataTypes) {
	return sequelize.define("Usernote", {
		id: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},

		// Schema definition is located here:
		// https://github.com/toolbox-team/reddit-moderator-toolbox/wiki/JSON:-usernotes
		username: DataTypes.STRING,
		n: DataTypes.STRING,
		t: DataTypes.TIME,
		m: DataTypes.STRING,
		l: DataTypes.STRING,
		w: DataTypes.STRING
	});
}