module.exports = {
    ver: {type: "number", positive: "true", integer: "true"},
    domainTags: [
        {type: "array", empty: true, items: {
            type: "object", props: {
                name: "string", 
                color: "string"
            }
        }},
        {type: "string"}
    ],
    removalReasons: {type: "object", props: {
        logreason: {type: "string", optional: true},
        pmsubject: {type: "string", optional: true},
        header: {type: "string", optional: true},
        footer: {type: "string", optional: true},
        logsub: {type: "string", optional: true},
        logtitle: {type: "string", optional: true},
        bantitle: {type: "string", optional: true},
        getfrom: {type: "string", optional: true},
        reasons: {type: "array", empty: true, items: {
            type: "object", props: {
                text: "string",
                flairText: "string",
                flairCSS: "string",
                title: "string"
            }
        }}
    }},
    modMacros: [
        {type: "array", empty: true, items: {
            type: "object", props: {
                text: "string",
                title: "string",
                distinguish: "boolean",
                ban: "boolean",
                mute: "boolean",
                remove: "boolean",
                approve: "boolean",
                lockthread: "boolean",
                sticky: "boolean",
                archivemodmail: "boolean",
                highlightmodmail: "boolean"
            }
        }},
        {type: "string"}
    ],
    usernoteColors: [
        {type: "array", empty: true, items: {
            type: "object", props: {
                key: "string",
                text: "string",
                color: "string"
            }
        }},
        {type: "string"}
    ],
    banMacros: [
        {type: "object", props: {
            banNote: "string",
            banMessage: "string"
        }},
        {type: "string"}
    ]
};