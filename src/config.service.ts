import { Service, Context, Errors } from "moleculer";
import * as Validator from "fastest-validator";

import * as Interfaces from "./interfaces"
const toolboxConfigSchema = require("./toolboxConfigSchema");

class ToolboxConfigService extends Service {
	protected v;
	protected tbSchema;

	constructor(broker) {
		super(broker);

		this.parseServiceSchema({
			name: "toolbox.config",
			version: 1,
			dependencies: [
				{name: "reddit.wiki", version: 1}
			],
			actions: {
				getConfig: {
					name: "getConfig",
					cache: true,
					params: {
						subreddit: "string",
						unescape: {type: "boolean", optional: true}
					}, 
					handler: this.getConfig
				},
				setConfig: {
					name: "setConfig",
					params: {
						subreddit: "string", 
						config: "object",
						escape: {type: "string", optional: true}
					},
					handler: this.setConfig
				}
			},
			events: {
				"toolbox.config.clearCache": this.clearCache
			},
			created: this.serviceCreated
		});
	}

	/**
	 * Retrieves the Toolbox configuration from Reddit.
	 * @function
	 * @static
	 * @name toolbox.config.getConfig
	 * @param {string} subreddit - The subreddit to retrieve the config from
	 * @param {boolean} [unescape=true] - Whether the escaped HTML strings should be unescaped
	 * @returns Parsed toolbox config
	 */
	async getConfig(ctx: Context) {
		let tbConfig = await this.broker.call("v1.reddit.wiki.getPage", {
			subreddit: ctx.params.subreddit, 
			page: "toolbox"
		});

		tbConfig = JSON.parse(tbConfig.content_md);

		if (ctx.params.unescape || ctx.params.unescape === undefined) {
			let macros = tbConfig.modMacros.map(async macro => {
				macro.text = unescape(macro.text);
				return macro;
			});
			let reasons = tbConfig.removalReasons.reasons.map(async reason => {
				reason.text = unescape(reason.text);
				return reason;
			});
			let escaped = await Promise.all([Promise.all(macros), Promise.all(reasons)]);
			tbConfig.modMacros = escaped[0];
			tbConfig.removalReasons.header = unescape(tbConfig.removalReasons.header);
			tbConfig.removalReasons.footer = unescape(tbConfig.removalReasons.footer);
			tbConfig.removalReasons.reasons = escaped[1];
		}

		tbConfig = JSON.parse(JSON.stringify(tbConfig));

		return tbConfig
	}

	/**
	 * Retrieves the Toolbox configuration from Reddit.
	 * @function
	 * @static
	 * @name toolbox.config.getConfig
	 * @param {string} subreddit - The subreddit to retrieve the config from
	 * @param {object} config - The new config. This gets validated against the config as of Oct 20th, 2018
	 * @param {boolean} [escape=true] - Whether the strings that need to be escaped are already escaped or not
	 * @returns Reddit API response from the page set, or 
	 */
	async setConfig(ctx: Context) {
		let tbConfig: Interfaces.ToolboxConfig = ctx.params.config;
		let validateResults = this.tbSchema(tbConfig)

		// Validate schema to make sure it's not malformed or anything
		if(validateResults) {
			// Escape the following strings if they need to be escaped
			if(ctx.params.escape || ctx.params.escape === undefined) {
				tbConfig.removalReasons.header = escape(tbConfig.removalReasons.header);
				tbConfig.removalReasons.footer = escape(tbConfig.removalReasons.footer);

				for(let i = 0; i < tbConfig.removalReasons.reasons.length; i++) {
					tbConfig.removalReasons.reasons[i].text = escape(tbConfig.removalReasons.reasons[i].text);
				}

				for(let i = 0; i < tbConfig.modMacros.length; i++) {
					tbConfig.modMacros[i].text = escape(tbConfig.modMacros[i].text);
				}
			}

			let response = await this.broker.call("v1.reddit.wiki.setPage", {
				subreddit: ctx.params.subreddit,
				page: "toolbox",
				text: JSON.stringify(tbConfig),
				reason: "Updated Toolbox config"
			});

			this.broker.broadcast("toolbox.config.clearCache");
		} else {
			throw new Errors.MoleculerClientError("Toolbox configuration validation failed, you dingus", 400, "VALIDATION_ERROR", validateResults);
		}
	}

	async clearCache() {
		await this.broker.cacher.clean("toolbox.config.getConfig");
	}

	async serviceCreated() {
		this.v = new Validator(); 
		this.tbSchema = this.v.compile(toolboxConfigSchema);
	}
}

module.exports = ToolboxConfigService;