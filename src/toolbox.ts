import { ServiceBroker } from "moleculer";

let broker = new ServiceBroker({
	nodeID: process.env.TRANSPORT_NODE_ID || undefined,
	transporter: { 
		type: "TCP",
		cacher: {
			type: "Memory",
			options: {
				ttl: 3600
			}
		},
		options: {
			udpBindAddress: process.env.TRANSPORT_BIND_ADDRESS || undefined
		}
	}
});

broker.loadServices(__dirname);
broker.start();