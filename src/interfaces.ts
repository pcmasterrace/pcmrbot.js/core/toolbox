// Interface definitions
// Up to date as of Oct 20, 2018
export interface ToolboxConfig {
	ver: number,
	domainTags: DomainTag[],
	removalReasons: RemovalReasons,
	modMacros: ModMacro[],
	usernoteColors: UsernoteColor[],
	banMacros: {
		banNote: string,
		banMessage: string
	}
}

export interface DomainTag {
	name: string,
	color: string
};

export interface RemovalReasons {
	pmsubject: string,
	logReason: string,
	header: string,
	footer: string,
	logsub: string,
	logtitle: string,
	bantitle: string,
	reasons: RemovalReason[]
}

export interface RemovalReason {
	text: string,
	flairText: string,
	flairCSS: string, 
	title: string
}

export interface ModMacro {
	text: string,
	title: string,
	distinguish: boolean,
	ban: boolean,
	mute: boolean,
	remove: boolean,
	approve: boolean,
	lockthread: boolean,
	sticky: boolean,
	archivemodmail: boolean,
	highlightmodmail: boolean
}

export interface UsernoteColor {
	key: string,
	text: string,
	color: string
}

export interface Usernote {
    n: string,
    t: number,
    m: number,
    l: string,
    w: string
}