# Toolbox

This is a PCMRBot.js module that performs common tasks associated with Reddit Toolbox. 

At the current time, it is suggested that only one instance of the Toolbox module runs per cluster. It should be possible to run multiple instances concurrently, however. 

## Dependencies

This module has the following dependencies: 

* [`core/reddit`](https://gitlab.com/pcmasterrace/pcmrbot.js/core/utilities), version 1

## Public methods

Check the specified file within the `src` directory for specific implementation details.

* Toolbox configuration (`config.service.ts`)
  * `v1.toolbox.config.getConfig` - Retrieves and parses the Toolbox configuration
  * `v1.toolbox.config.setConfig` - Updates the Toolbox configuration on Reddit
* Removal reasons (`removalReasons.service.ts`)
  * `v1.toolbox.removalReasons.renderMessage` - Renders a removal message using the Toolbox configuration
* Usernotes (`usernotes.service.ts`)
  * `v1.toolbox.usernotes.getUsernotes` - Retrieves the usernotes for a subreddit or a specific user
  * `v1.toolbox.usernotes.addUsernote` - Adds a usernote to a specified user
  * `v1.toolbox.usernotes.removeUsernote` - Removes a specified usernote from a specified user

## Environment variables

The following environment variables should be set prior to launch. 

* Database options ([options passed directly into Sequelize](http://docs.sequelizejs.com/manual/installation/usage.html))
  * `TOOLBOX_DB_DIALECT` - The database dialect to use
  * `TOOLBOX_DB_HOST` - The database hostname to connect to
  * `TOOLBOX_DB_NAME` - The database name to connect to
  * `TOOLBOX_DB_USERNAME` - The database username
  * `TOOLBOX_DB_PASSWORD` - The database password
  * `TOOLBOX_DB_PORT` - The database port
  * `TOOLBOX_DB_PATH` - (SQLite only) The database path on the filesystem
  * (optional) `TOOLBOX_DB_DROP_TABLES_ON_START` - Whether the tables used should be cleaned at launch. Defaults to false
* Broker options
  * (optional) `TRANSPORT_NODE_ID` - The node ID that should be used for connecting to the rest of the cluster. Defaults to the system hostname plus a random number string. 
  * (optional) `TRANSPORT_BIND_ADDRESS` - The network address that the service broker should bind to. Defaults to all available addresses across all network interfaces.